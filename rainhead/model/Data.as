package rainhead.model 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Mc
	 */
	public class Data
	{		
		//Screen config
		public static const screenWidth:int = 640;
		public static const screenHeight:int = 480;
		public static const screenColor:uint = 0x00000000;
		
		//Bitmap config
		public static const pixelsWidth:int = 10000;
		public static const pixelsHeight:int = 10000;
		private const count:int = 100000;
		
		//Source files
		[Embed(source = "../../../assets/snowflakes-a.png")]
		private static const snowAlpha:Class;
		[Embed(source = "../../../assets/snowflakes-b.png")]
		private static const snowBlue:Class;
		[Embed(source = "../../../assets/snowflakes-g.png")]
		private static const snowGreen:Class;
		[Embed(source = "../../../assets/snowflakes-y.png")]
		private static const snowYellow:Class;
		
		//Pixel vars
		private var pixelsVector:Vector.<BitmapData>;
		private var pixels:BitmapData;
		//Speed vars
		private const ms:int = 1000;
		private var vx:int = 20;
		private var vy:int = 30;
		//Start position
		private var _x:Number = pixelsWidth >> 1;
		private var _y:Number = pixelsHeight >> 1;
		
		public function Data():void {
			initPixels();
			fillPixels();
		}
		
		private function initPixels():void 
		{
			pixelsVector = new Vector.<BitmapData>();
			pixelsVector.push((new snowBlue() as Bitmap).bitmapData);
			pixelsVector.push((new snowGreen() as Bitmap).bitmapData);
			pixelsVector.push((new snowYellow() as Bitmap).bitmapData);
			pixels = new BitmapData(pixelsWidth, pixelsHeight, true, screenColor);
		}
		
		private function fillPixels():void 
		{
			//Setup alpha
			var alpha:BitmapData = (new snowAlpha() as Bitmap).bitmapData;
			var alphaWidth:int = alpha.width;
			var alphaHeight:int = alpha.height;
			//Setup vector
			var length:int = pixelsVector.length;
			var r:Rectangle = new Rectangle(0, 0, 32, 32);
			var p:Point = new Point();
			var a:Point = new Point();
			//Fill vector
			for (var i:int = 0; i < count; i++) 
			{
				p.x = Math.random() * (pixelsWidth - alphaWidth);
				p.y = Math.random() * (pixelsHeight - alphaHeight);
				pixels.copyPixels(pixelsVector[int(Math.random() * length)], r, p, alpha, a, true); 
			}
		}
		
		public function getPixels():BitmapData
		{
			return pixels;
		}
		
		public function increaseX():void 
		{
			vx += 10;
		}
		
		public function decreaseX():void 
		{
			vx -= 10;
		}
		
		public function increaseY():void 
		{
			vy += 10;
		}
		
		public function decreaseY():void 
		{
			vy -= 10;
		}
		
		public function updateModel(diff:int):void 
		{
			//Update x of animation
			_x -= vx * diff / ms;
			if (_x < 0) _x += pixelsWidth;
			else if (_x > pixelsWidth) _x -= pixelsWidth;
			//Update y of animation
			_y -= vy * diff / ms;
			if (_y < 0) _y += pixelsHeight;
			else if (_y > pixelsHeight) _y -= pixelsHeight;
		}
		
		public function get x():Number 
		{
			return _x;
		}
		
		public function get y():Number
		{
			return _y;
		}
	}
}