package rainhead
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.utils.getTimer;
	import net.hires.debug.Stats;
	import rainhead.controller.Controller;
	import rainhead.model.Data;
	import rainhead.view.Screen;
	
	/**
	 * ...
	 * @author Mc
	 */
	public class Main extends Sprite 
	{
		private var data:Data;
		private var screen:Screen;
		private var controller:Controller;
		private var time:int = 0;
		private var diff:int;
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			data = new Data();
			this.stage.addChild(screen = new Screen(data));
			controller = new Controller(data);
			this.stage.addChild(new Stats());
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKey);
		}
		
		private function onEnterFrame(e:Event):void 
		{
			if (time == 0) {
				time = getTimer();
			}
			else {
				diff = getTimer() - time;
				//Update model with time passed
				data.updateModel(diff);
				//Update screen
				screen.updateScreen();
				time = getTimer();
			}
		}
		
		private function onKey(e:KeyboardEvent):void 
		{
			//Capture input
			controller.updateInput(e.keyCode);
		}
	}
}