package rainhead.controller 
{
	import flash.events.EventDispatcher;
	import flash.ui.Keyboard;
	import rainhead.model.Data;
	/**
	 * ...
	 * @author Mc
	 */
	public class Controller
	{
		private var data:Data;
		
		public function Controller(data:Data) 
		{
			this.data = data;
		}		
		
		public function updateInput(keyCode:uint):void 
		{
			switch (keyCode) 
			{
				case Keyboard.UP:
					data.decreaseY();
					break;
				case Keyboard.DOWN:
					data.increaseY();
					break;
				case Keyboard.RIGHT:
					data.increaseX();
					break;
				case Keyboard.LEFT:
					data.decreaseX();
					break;
			}
		}
	}
}