package rainhead.view 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import rainhead.model.Data;
	
	/**
	 * ...
	 * @author Mc
	 */
	public class Screen extends Bitmap 
	{	
		private var data:Data;
		private var bitmapPixels:BitmapData;
		private var r:Rectangle;
		private var p:Point;
		private var widthLimit:int;
		private var heightLimit:int;
		
		public function Screen(data:Data) 
		{
			super(new BitmapData(Data.screenWidth, Data.screenHeight, false, Data.screenColor));
			this.data = data;
			this.bitmapPixels = data.getPixels();
			p = new Point();
			widthLimit = Data.pixelsWidth - Data.screenWidth;
			heightLimit = Data.pixelsHeight - Data.screenHeight;
		}
		
		public function updateScreen():void 
		{
			//Sample pixels from model
			var x:int = data.x;
			var y:int = data.y;
			//Clear screen
			var dx:int = 0;
			var dy:int = 0;
			r = new Rectangle(0, 0, Data.screenWidth, Data.screenHeight);
			this.bitmapData.fillRect(r, Data.screenColor);
			//Fill
			if (x > widthLimit) dx = x - widthLimit;
			if (y > heightLimit) dy = y - heightLimit;
			//Attempts to draw additional rectangles, when position + rect exceeds pixels
			if (dx != 0 && dy != 0) {
				p.x = Data.screenWidth - dx;
				p.y = Data.screenHeight - dy;
				r = new Rectangle(0, 0, dx, dy);
				this.bitmapData.copyPixels(bitmapPixels, r, p);
			}
			if (dy != 0) {
				p.x = 0;
				p.y = Data.screenHeight - dy;
				r = new Rectangle(x, 0, Data.screenWidth - dx, dy);
				this.bitmapData.copyPixels(bitmapPixels, r, p);
			}
			if (dx != 0) {
				p.x = Data.screenWidth - dx;
				p.y = 0;
				r = new Rectangle(0, y, dx, Data.screenHeight - dy);
				this.bitmapData.copyPixels(bitmapPixels, r, p);
			}
			p.x = 0;
			p.y = 0;
			r = new Rectangle(x, y, Data.screenWidth - dx, Data.screenHeight - dy);
			this.bitmapData.copyPixels(bitmapPixels, r, p);
		}
	}
}